> Brandon Henao


## Advertencia de rendimiento
El despliegue se encuentra en un entorno gratuito y por tanto limitado. Pueden haber retrasos en las respuestas o peticiones http perdidas.


## Herramientas
- Visual studio code


## Tecnologías
- Pusher
- Node
- nodemon (cuando se esta en modo desarrollo refresca los cambios realizados al instante, sin necesidad de para el servidor y volver a subirlo)
- [![express-validator](https://express-validator.github.io/docs/custom-error-messages.html)] (validar las entradas de los parametros)
- cors
- link-module-alias, para rutas absolutas




## Comandos
- npm init , para iniciar el proyecto node
- npm install , install todas las dependencias
- npm run postinstall , para instalar link-module-alias
- node index.js , para poner en funcionamiento el proyecto
- nodemon index.js  , para poner en funcionamiento el proyecto
